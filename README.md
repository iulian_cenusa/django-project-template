# README #

Django project template.
If you don't have django installed please install all required dependencies by using requirements.

### How to use ###

* Run:
* django-admin startproject name_of_project --template project_name_path
* Where name_of_project is the name of the new project you'll create
* and project_name_path is the path to the folder project_name from this repo.

### Advantages ###

* Authentification system already set-up ( login/logout/register users )
* Forms use django-crispy-forms plugin
* Administration area ( Please change default credentials for user admin )
* Landing page already set-up with Bootstrap 5 navbar and functionality. 
