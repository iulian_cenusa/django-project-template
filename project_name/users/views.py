from django.shortcuts import render
from django.views import  generic
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy

# User registration Form
class UsrRegView(generic.CreateView):
    form_class = UserCreationForm
    template_name = 'registration/register.html'
    success_url = reverse_lazy('login')

# Basic Hoempage
def home(request):
    if request.user.is_authenticated:
        pass
    else:
        pass
    return render(request,'users/home.html',{
        
            })
